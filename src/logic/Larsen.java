package logic;

import models.values.DiscreteValue;
import models.values.Value;

public class Larsen {

    public static class Implication implements logic.interfaces.Implication {
        @Override
        public Value getValue(Value a, Value b) {
            return new DiscreteValue(a.getValue()*b.getValue());
        }
    }
}
