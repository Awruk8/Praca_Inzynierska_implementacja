package logic;

import models.values.DiscreteValue;
import models.values.Value;

public class Drastic {

    public static class TNorm implements logic.interfaces.TNorm {

        @Override
        public Value getValue(Value a, Value b) {
            if(Math.max(a.getValue(), b.getValue()) == 1) {
                return new DiscreteValue(Math.min(a.getValue(), b.getValue()));
            }  else return new DiscreteValue(0);
        }
    }

    public static class SNorm implements logic.interfaces.SNorm {

        @Override
        public Value getValue(Value a, Value b) {
            if(Math.min(a.getValue(),b.getValue()) == 0) {
                return new DiscreteValue(Math.max(a.getValue(), b.getValue()));
            } else return new DiscreteValue(1);
        }
    }
}
