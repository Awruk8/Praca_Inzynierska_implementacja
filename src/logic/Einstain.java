package logic;

import models.values.DiscreteValue;
import models.values.Value;

public class Einstain {

    public static class TNorm implements logic.interfaces.TNorm {

        @Override
        public Value getValue(Value a, Value b) {
            return new DiscreteValue((a.getValue()*b.getValue()/(2 - ( a.getValue() + b.getValue() - a.getValue()*b.getValue()))));
        }
    }

    public static class SNorm implements logic.interfaces.SNorm {

        @Override
        public Value getValue(Value a, Value b) {
            return new DiscreteValue((a.getValue() + b.getValue())/ (1 + a.getValue()*b.getValue()));
        }
    }
}
