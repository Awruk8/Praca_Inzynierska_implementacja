package logic;

import models.values.DiscreteValue;
import models.values.Value;
import operators.Approximation;
import operators.Operator;

public class GeodelLogic extends Operator {

    public GeodelLogic() {
        super(new TNorm(), new SNorm(), new Implication(), Approximation.DOWN);
    }

    public static class TNorm implements logic.interfaces.TNorm {

        @Override
        public Value getValue(Value a, Value b) {
            if(a == null) return new DiscreteValue(Math.min(1, b.getValue()));
            if(b == null) return new DiscreteValue(Math.min(a.getValue(), 1));
            return new DiscreteValue(Math.min(a.getValue(), b.getValue()));
        }
    }

    public static class SNorm implements logic.interfaces.SNorm {

        @Override
        public Value getValue(Value a, Value b) {
            if(a == null) return new DiscreteValue(Math.max(0, b.getValue()));
            if(b == null) return new DiscreteValue(Math.max(a.getValue(), 0));
            return new DiscreteValue(Math.max(a.getValue(), b.getValue()));
        }
    }

    public static class Implication implements logic.interfaces.Implication {

        @Override
        public Value getValue(Value a, Value b) {
            return null;
        }
    }
}
