package logic;

import models.values.DiscreteValue;
import models.values.Value;
import operators.Approximation;
import operators.Operator;

public class Mamdani extends Operator {

    public Mamdani() {
        super(new TNorm(), new SNorm(), new Implication(), Approximation.DOWN);
    }

    public static class TNorm implements logic.interfaces.TNorm {

        @Override
        public Value getValue(Value a, Value b) {
            return null;
        }
    }

    public static class SNorm implements logic.interfaces.SNorm {

        @Override
        public Value getValue(Value a, Value b) {
            return null;
        }
    }

    public static class Implication implements logic.interfaces.Implication {

        @Override
        public Value getValue(Value a, Value b) {
            return new DiscreteValue(Math.min(a.getValue(), b.getValue()));
        }
    }
}
