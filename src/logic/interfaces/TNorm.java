package logic.interfaces;

import models.values.Value;

public interface TNorm {

    Value getValue(Value a, Value b);
}
