package logic.interfaces;

import models.values.Value;

public interface SNorm {

    Value getValue(Value a, Value b);
}
