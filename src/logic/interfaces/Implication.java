package logic.interfaces;

import models.values.Value;

public interface Implication {

    Value getValue(Value a, Value b);
}
