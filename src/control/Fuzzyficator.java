package control;

import algorithms.Algorithm;
import lists.AttributesSpecifications;
import lists.KeyAttributeSpecifications;
import models.*;
import models.Object;
import models.values.DiscreteValue;
import models.values.FuzzyValue;
import models.values.Value;

import java.util.ArrayList;
import java.util.List;

public class Fuzzyficator {

    public static List<Record> fuzzyInput(List<Record> records, AttributesSpecifications attributesSpecifications, KeyAttributeSpecifications keyAttributeSpecyfications) {

        List<Record> fuzzyfiedRecords = new ArrayList<>();

        for(Record record : records) {
            Record fuzzyfiedRecord = new Record(new Object(record.getObject().getId()), new ArrayList<>(), null);
            //region fuzzy attributes
            ArrayList<AttributeEvaluation> fuzzyAttributeEvaluations = new ArrayList<>();

            for(AttributeEvaluation discreteEvaluation : (List<AttributeEvaluation>) record.getAttributeEvaluations()) {

                AttributesSpecifications.AttributeSpecification attributeSpecs = Algorithm.getAttributeSpecifications(records).getSpecsOfAttribute(discreteEvaluation.getAttribute());

                /*
                calculate membership to 3 classes (LOW,MEDIUM,HIGH) based on statistics of attribute evaluations
                 */
                if(attributeSpecs.getStandardDeviation() == 0) {
                    int i = 0;
                }
                FuzzyValue value = calculateMembership(discreteEvaluation, attributesSpecifications.getSpecsOfAttribute(discreteEvaluation.getAttribute()));
                AttributeEvaluation newPair = new AttributeEvaluation<>(discreteEvaluation.getAttribute(), value);
                fuzzyAttributeEvaluations.add(newPair);
            }
            fuzzyfiedRecord.setAttributeEvaluations(fuzzyAttributeEvaluations);
            //endregion fuzzy attributes

            //region fuzzy key attribute
            KeyAttribute keyAttribute =  new KeyAttribute(record.getKeyAttribute().getName());

            double meanValue = keyAttributeSpecyfications.getMeanValue();
            double standardDeviation = keyAttributeSpecyfications.getStandardDeviation();

            double low, medium, high;

            double discreteValue = record.getKeyAttribute().getDiscreteValue().getValue();

            low = discreteValue >= meanValue ? 0 :
                    (1 - Math.exp(
                            -(Math.pow(discreteValue - meanValue, 2))
                                    / (2*Math.pow(standardDeviation,2))));

            medium = Math.exp(
                    -(Math.pow(discreteValue - meanValue, 2))
                            / (2*Math.pow(standardDeviation,2)));

            high = discreteValue <= meanValue ? 0 :
                    (1 - Math.exp(
                            -(Math.pow(discreteValue - meanValue, 2))
                                    / (2*Math.pow(standardDeviation,2))));

            FuzzyValue value = new FuzzyValue(low,medium,high);
            //endregion fuzzy key attribute

            keyAttribute.setFuzyyValue(value);
            keyAttribute.setTestValue(record.getKeyAttribute().getTestValue());
            double cog = 0;
            String valueClass = value.toString();
            switch (valueClass) {
                case "HIGH": cog = keyAttributeSpecyfications.getMaxValue(); break;
                case "MEDIUM": cog = (keyAttributeSpecyfications.getMaxValue()+keyAttributeSpecyfications.getMinValue())/2; break;
                case "LOW": cog = keyAttributeSpecyfications.getMinValue(); break;
            }
            keyAttribute.setName(valueClass);
            keyAttribute.setCog(new DiscreteValue(cog));
            fuzzyfiedRecord.setKeyAttribute(keyAttribute);
            fuzzyfiedRecords.add(fuzzyfiedRecord);
        }
        return fuzzyfiedRecords;
    }

    public static Record fuzzyCrisp(Record record, AttributesSpecifications attributeSpecyfications) {
        AttributeEvaluation attributeEvaluation = (AttributeEvaluation) record.getAttributeEvaluations().get(0);
        if(attributeEvaluation.getValue() instanceof FuzzyValue) {
            return record;
        }
        List<AttributeEvaluation> fuzzyAttributeEvaluation = new ArrayList<>();
        for(AttributeEvaluation discreteEvaluation : (List<AttributeEvaluation>) record.getAttributeEvaluations()) {
            FuzzyValue value = calculateMembership(discreteEvaluation, attributeSpecyfications.getSpecsOfAttribute(discreteEvaluation.getAttribute()));
            AttributeEvaluation fuzzyEvaluation = new AttributeEvaluation(discreteEvaluation.getAttribute(), value);
            fuzzyAttributeEvaluation.add(fuzzyEvaluation);
        }
        record.setAttributeEvaluations(fuzzyAttributeEvaluation);
        return record;
    }

    private static FuzzyValue calculateMembership(AttributeEvaluation discreteEvaluation, AttributesSpecifications.AttributeSpecification attributeSpecs) {
        double meanValue = attributeSpecs.getMeanValue();
        double standardDeviation = attributeSpecs.getStandardDeviation();

        double low, medium, high;

        double discreteValue = discreteEvaluation.getValue().getValue();

        low = discreteValue >= meanValue ? 0 :
                (1 - Math.exp(
                        -(Math.pow(discreteValue - meanValue, 2))
                                / (2*Math.pow(standardDeviation,2))));

        medium = Math.exp(
                -(Math.pow(discreteValue - meanValue, 2))
                        / (2*Math.pow(standardDeviation,2)));

        high = discreteValue <= meanValue ? 0 :
                (1 - Math.exp(
                        -(Math.pow(discreteValue - meanValue, 2))
                                / (2*Math.pow(standardDeviation,2))));

        if(standardDeviation == 0) {
            low = 0.1;
            medium = 0.8;
            high = 0.1;
        }
        if(discreteEvaluation.getAttribute().getName().equals("waga")) {
            int i = 0;
        }
        FuzzyValue fuzzyfiedValue = new FuzzyValue(low,medium,high);

        return fuzzyfiedValue;
    }


}
