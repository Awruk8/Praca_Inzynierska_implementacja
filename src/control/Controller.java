package control;

import algorithms.Algorithm;
import com.google.gson.Gson;
import javafx.util.Pair;
import lists.*;
import models.MamdaniModel;
import models.Record;
import models.Rule;
import models.values.DiscreteValue;
import models.values.FuzzyValue;
import models.values.Value;
import operators.Operator;
import utils.ConsoleWriter;

import java.util.ArrayList;
import java.util.List;

public class Controller extends MamdaniModel {

    private List<Rule> rules;
    private AttributesSpecifications attributeSpecifications;
    private KeyAttributeSpecifications keyAttributeSpecifications;

    public Controller(Operator operator) {
        this.operator = operator;
    }

    @Override
    public void setLearningSet(LearningSet records) {
        /*
        Obtain attributes specifications to provide source of fuzzy classes
         */
        this.attributeSpecifications = Algorithm.getAttributeSpecifications(records);
        this.keyAttributeSpecifications = Algorithm.getKeyAttributeSpecifications(records);

        /*
        fuzzyfy input
         */
        List<Record> fuzzyficationResult = Fuzzyficator.fuzzyInput(records, this.attributeSpecifications, this.keyAttributeSpecifications);
//        String json = new Gson().toJson(fuzzyficationResult);
        this.learningSet = new LearningSet(fuzzyficationResult);
   }

    /**
     * Generate rules based on learning set
     */
    @Override
    public void generateRules() {

        DecisionList decisionList = Algorithm.divideByDecisions(this.learningSet);

        IndifferenceList indifferenceList = Algorithm.indiferrenceDivide(this.learningSet);

        List<Record> normalized = Algorithm.normalize(this.learningSet, decisionList, indifferenceList);

        AttributesList[][] attDiffMatrix = Algorithm.getAttributeDifferenceMatrix(normalized);

        Reduct reduct = Algorithm.reduceAttributes(attDiffMatrix);

        attDiffMatrix = Algorithm.getAttributeDifferenceMatrix(normalized);

        ArrayList<List<Pair<Record,AttributesList>>> a = Algorithm.getImplicantMatrix(attDiffMatrix, this.learningSet);

        ImplicantList implicantList = Algorithm.generateImplicants(a,reduct, true);

        List<Rule> rules = Algorithm.generateRules(implicantList);
        this.rules = rules;
    }

    @Override
    public Value calculateOutputValue(Record record) {
        String json = new Gson().toJson(record);
        Record fuzzyfied = Fuzzyficator.fuzzyCrisp(record, attributeSpecifications);

        List<Pair<Rule, Value>> ruleValues = new ArrayList<>();
        Rule bestRule = null;
        Value bestValue = null;
        for (Rule rule : rules) {
            Value value = rule.calculateValue(fuzzyfied, this.operator);
            ruleValues.add(new Pair(rule, value));
            if(bestValue == null) {
                bestValue = value;
                bestRule = rule;
            } else if(bestValue.getValue() < value.getValue()) {
                bestValue = value;
                bestRule = rule;
            }
        }
        double outPutValue = 0;

        double sumOfRuleImplication = 0;
        double sumOfValues = 0;

        for(Pair<Rule,Value> ruleValue : ruleValues) {
            sumOfRuleImplication += operator.calculateImplication(ruleValue.getValue(), ruleValue.getKey().getKeyAttribute().getCog()).getValue();
            sumOfValues += ruleValue.getValue().getValue();
        }

        outPutValue = (sumOfRuleImplication/sumOfValues);//*constans; ?

        ConsoleWriter.writeDescision(record, outPutValue);
        return new DiscreteValue(outPutValue);
    }
}
