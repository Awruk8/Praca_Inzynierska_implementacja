package lists;

import javafx.util.Pair;
import models.AttributeEvaluation;
import models.Object;

import java.util.ArrayList;
import java.util.List;

public class IndifferenceList extends ArrayList<Pair<ArrayList<AttributeEvaluation>,ArrayList<Object>>>{

    public Pair<ArrayList<AttributeEvaluation>, ArrayList<Object>> getIndifference(List<AttributeEvaluation> attributeEvaluations) {
        for(int i =0; i < size(); i++) {
            boolean matches = true;
            List<AttributeEvaluation> current = get(i).getKey();
            for(int j = 0; j<attributeEvaluations.size(); j++) {
                if(!attributeEvaluations.get(j).getValue().isEqual(current.get(j).getValue())) matches = false;
            }
            if(matches) {
                return get(i);
            }
        }
        return null;
    }

    public void print() {
        System.out.print("{");
        for(int i = 0; i<size(); i++) {
            Pair<ArrayList<AttributeEvaluation>,ArrayList<Object>> pair = get(i);
            System.out.print("{");
            for(Object object : pair.getValue()) {
                System.out.print(object.toString());
            }
            System.out.print("}");
        }
        System.out.print("}\n");
    }
}
