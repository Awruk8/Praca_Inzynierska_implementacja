package lists;

import models.Record;
import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.Collection;

public class LearningSet extends ArrayList<Record> {

    public LearningSet(@NotNull Collection<? extends Record> c) {
        super(c);
    }
}
