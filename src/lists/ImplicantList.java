package lists;

import models.Implicant;

import java.util.ArrayList;

public class ImplicantList extends ArrayList<Implicant> {

    public boolean containsImplicant(Implicant implicant) {
        for(int i = 0; i<this.size(); i++) {
            Implicant toCheck = this.get(i);
            if(implicant.isSame(implicant)) return true;
        }
        return false;
    }
}
