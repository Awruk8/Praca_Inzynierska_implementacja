package lists;

import models.Attribute;

import java.util.ArrayList;

public class AttributesSpecifications extends ArrayList<AttributesSpecifications.AttributeSpecification> {

    public AttributeSpecification getSpecsOfAttribute(Attribute attribute) {
        for(int i =0 ; i<this.size(); i++) {
            if(this.get(i).getAttribute().equals(attribute)) return this.get(i);
        }
        return null;
    }

    public static class AttributeSpecification {

        private final Attribute attribute;
        private final double meanValue;
        private final double standardDeviation;

        public AttributeSpecification(Attribute attribute, double meanValue, double standardDeviation) {
            this.attribute = attribute;
            this.meanValue = meanValue;
            this.standardDeviation = standardDeviation;
        }

        public Attribute getAttribute() {
            return attribute;
        }

        public double getMeanValue() {
            return meanValue;
        }

        public double getStandardDeviation() {
            return standardDeviation;
        }

    }
}
