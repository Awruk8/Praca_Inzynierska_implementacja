package models;

import com.google.gson.annotations.SerializedName;

public class Attribute {

    @SerializedName("name")
    protected String name;

    public Attribute(String name) {
        this.name = name;
    }

    public boolean equals(Attribute other) {
        return this.name.equals(other.name);
    }

    public String getName() {
        return name;
    }

    @Override
    public String toString() {
        return getName();
    }
}
