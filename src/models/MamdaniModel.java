package models;

import lists.LearningSet;
import models.values.DiscreteValue;
import models.values.Value;
import operators.Operator;

import java.util.List;

public abstract class MamdaniModel {

    protected LearningSet learningSet;
    protected List<Rule> rules;
    protected Operator operator;

    protected abstract void setLearningSet(LearningSet learningSet);

    protected abstract void generateRules();

    protected abstract Value calculateOutputValue(Record record);
}
