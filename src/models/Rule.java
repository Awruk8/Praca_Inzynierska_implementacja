package models;

import javafx.util.Pair;
import lists.AttributesList;
import models.values.DiscreteValue;
import models.values.FuzzyValue;
import models.values.Value;
import operators.Operator;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Rule {

    private final KeyAttribute keyAttribute;
    private List<Pair<Object, List<List<List<AttributeEvaluation>>>>> elements;

    public Rule(KeyAttribute keyAttribute, List<Implicant> implicantList) {
        this.keyAttribute = keyAttribute;
        this.elements = new ArrayList<>();
        Map<Object, List<Implicant>> ruleElementsMap = new HashMap<>();
        for(Implicant implicant : implicantList) {
            List<Implicant> implicants = new ArrayList<>();
            for(Object obj : ruleElementsMap.keySet()) {
                if(obj.equals(implicant.getKeyAttribute())) implicants = ruleElementsMap.get(obj);
            }
            implicants.add(implicant);
            Object matchingKey = null;
            for(Object obj : ruleElementsMap.keySet()) {
                if(obj.equals(implicant.getKeyAttribute())) matchingKey = obj;
            }
            if(matchingKey == null) ruleElementsMap.put(implicant.getObject(), implicants);
            else ruleElementsMap.replace(matchingKey, implicants);
        }
        for(Object object : ruleElementsMap.keySet()) {
            Pair<Object, List<List<List<AttributeEvaluation>>>> what;
            List<List<List<AttributeEvaluation>>> sublist = new ArrayList<>();
            List<Implicant> relatedImplicants = ruleElementsMap.get(object);
            for(Implicant implicant : relatedImplicants) {
                List<List<AttributeEvaluation>> listOfMyPairs = new ArrayList<>();
                List<AttributesList> listOfImplicantElements = implicant.getElements();
                for(AttributesList attributesList : listOfImplicantElements) {
                    List<AttributeEvaluation> attributeEvaluations = new ArrayList<>();
                    List<Attribute> implicantElementAttributes = attributesList;
                    for(Attribute attribute : implicantElementAttributes) {
                        AttributeEvaluation attributeEvaluation = implicant.getRecord().getAttributeEvaluation(attribute);
                        attributeEvaluations.add(attributeEvaluation);
                    }
                    listOfMyPairs.add(attributeEvaluations);
                }
                sublist.add(listOfMyPairs);
            }
            what = new Pair<>(object, sublist);
            if(!what.getValue().isEmpty())// && elements.size() < 3)
                elements.add(what);
        }
    }

    public KeyAttribute getKeyAttribute() {
        return keyAttribute;
    }

    public List<Pair<Object, List<List<List<AttributeEvaluation>>>>> getElements() {
        return elements;
    }

    public Value calculateValue(Record record, Operator operator) {

        Value value = null;
        for (Pair<Object, List<List<List<AttributeEvaluation>>>> ruleElement : elements) {

            List<List<List<AttributeEvaluation>>> sublistA = ruleElement.getValue();
            sublistA:
            for (List<List<AttributeEvaluation>> sublistB : sublistA) {
                Value sublistBvalue = null;
                sublistB:
                for (List<AttributeEvaluation> sublistC : sublistB) {
                    Value sublistCvalue = null;
                    sublistC:
                    for (AttributeEvaluation attributeEval : sublistC) {
                        AttributeEvaluation currentEval = record.getAttributeEvaluation(attributeEval.getAttribute());
                        FuzzyValue fuzzyRuleValue = (FuzzyValue) attributeEval.getValue();
                        String level = fuzzyRuleValue.toString();
                        switch (level) {
                            case "HIGH":
                                sublistCvalue = operator.calculateSNorm(sublistCvalue, new DiscreteValue(((FuzzyValue) currentEval.getValue()).getHigh()));
                                break;
                            case "MEDIUM":
                                sublistCvalue = operator.calculateSNorm(sublistCvalue, new DiscreteValue(((FuzzyValue) currentEval.getValue()).getMedium()));
                                break;
                            case "LOW":
                                sublistCvalue = operator.calculateSNorm(sublistCvalue, new DiscreteValue(((FuzzyValue) currentEval.getValue()).getLow()));
                                break;
                            default:
                                System.out.println("WRONG CLASS OF ARGUMENT OF RULE");
                                return null;
                        }
                    }
                    sublistBvalue = operator.calculateTNorm(sublistCvalue, sublistBvalue);
                }
                value = operator.calculateSNorm(value, sublistBvalue);
            }
        }
        return value;
    }
}
