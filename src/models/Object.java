package models;

import com.google.gson.annotations.SerializedName;

public class Object {

    @SerializedName("id")
    private int id;

    public Object(int id) {
        this.id = id;
    }

    public int getId() {
        return id;
    }

    @Override
    public String toString() {
        return "Object id: "+String.valueOf(id);
    }

    public boolean equals(Object other) {
        return this.id == other.id;
    }
}
