package models;

import com.google.gson.annotations.SerializedName;
import models.values.DiscreteValue;
import models.values.FuzzyValue;
import models.values.Value;

import java.lang.*;

public class KeyAttribute extends Attribute {

    @SerializedName("value")
    private DiscreteValue discreteValue;

    private FuzzyValue fuzyyValue;

    private DiscreteValue cog;

    private int testValue;

    public KeyAttribute(String name) {
        super(name);
    }

    public boolean equals(KeyAttribute keyAttribute) {
        return keyAttribute.name.equals(this.name);
    }

    @Override
    public String toString() {
        String value = fuzyyValue != null ? fuzyyValue.toString() : discreteValue != null ? discreteValue.toString() : "";
        return ": "+name+" "+value;
    }

    public FuzzyValue getFuzyyValue() {
        return fuzyyValue;
    }

    public void setFuzyyValue(FuzzyValue fuzyyValue) {
        this.fuzyyValue = fuzyyValue;
    }

    public DiscreteValue getDiscreteValue() {
        return discreteValue;
    }

    public DiscreteValue getCog() {
        return this.cog;
    }

    public void setCog(DiscreteValue cog) {
        this.cog = cog;
    }

    public void setDiscreteValue(DiscreteValue value) {
        this.discreteValue = value;
    }

    public void setName(String newName) {
        this.name = newName;
    }

    public void setTestValue(int keyValue) {
        this.testValue = keyValue;
    }

    public int getTestValue() {
        return testValue;
    }
}
