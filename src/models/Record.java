package models;

import com.google.gson.annotations.SerializedName;
import lists.AttributesList;
import models.values.Value;

import java.lang.*;
import java.util.List;

public class Record<T extends Value> {

    @SerializedName("item")
    protected models.Object item;
    @SerializedName("attributeEvaluations")
    protected List<AttributeEvaluation<T>> attributeEvaluations;
    @SerializedName("keyAttribute")
    protected KeyAttribute keyAttribute;

    public Record(models.Object item, List<AttributeEvaluation<T>> attributeEvaluations, KeyAttribute keyAttribute) {
        this.item = item;
        this.attributeEvaluations = attributeEvaluations;
        this.keyAttribute = keyAttribute;
    }



    public models.Object getObject() {
        return item;
    }

    public KeyAttribute getKeyAttribute() {
        return keyAttribute;
    }

    @Override
    public String toString() {
        return item.toString() + " " + keyAttribute.toString();
    }

    public AttributesList getDifferentAttributes(Record other) {
        AttributesList attributes = new AttributesList();
        for(int i=0;i<getAttributeEvaluations().size(); i++) {
            AttributeEvaluation thisAttribute = getAttributeEvaluations().get(i);
            AttributeEvaluation otherAttribute = ((List<AttributeEvaluation>)other.getAttributeEvaluations()).get(i);
            if(!(thisAttribute.getValue()).isEqual(otherAttribute.getValue())) {
                attributes.add(thisAttribute.getAttribute());
            }
        }
        return attributes;
    }

    public AttributeEvaluation getAttributeEvaluation(Attribute attribute) {
        for(AttributeEvaluation pair : this.attributeEvaluations) {
            if(pair.getAttribute().equals(attribute)) return pair;
        }
        return null;
    }

    public void setAttributeEvaluations(List<AttributeEvaluation<T>> attributeEvaluations) {
        this.attributeEvaluations = attributeEvaluations;
    }

    public List<AttributeEvaluation<T>> getAttributeEvaluations() {
        return attributeEvaluations;
    }

    public void setKeyAttribute(KeyAttribute keyAttribute) {
        this.keyAttribute = keyAttribute;
    }
}
