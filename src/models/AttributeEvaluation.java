package models;

import com.google.gson.annotations.SerializedName;
import models.values.DiscreteValue;
import models.values.Value;

public class AttributeEvaluation<T extends Value> {

    @SerializedName("attribute")
    private Attribute attribute;
    @SerializedName("value")
    private T value;

    public AttributeEvaluation(Attribute attribute, T value) {
        this.attribute = attribute;
        this.value = value;
    }

    public Attribute getAttribute() {
        return attribute;
    }

    public T getValue() {
        return value;
    }

    @Override
    public String toString() {
        return attribute.toString() + ": " + value.toString();
    }
}
