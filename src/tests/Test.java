package tests;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.sun.org.apache.regexp.internal.RE;
import lists.LearningSet;
import models.Attribute;
import models.AttributeEvaluation;
import models.KeyAttribute;
import models.Object;
import models.values.DiscreteValue;
import org.omg.CORBA.INTERNAL;

import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class Test {

    public static final String[] TABLETS = new String[]{
            "ada_chmielewska",
            "adam_kucharski",
            "adrian_dziedzic",
            "aleksandra_krzywanska",
            "andrzej_filipowicz",
            "antoni_nguyen",
            "bartosz_hamielec",
            "bartosz_szalaj",
            "daniel_calka",
            "dawid_pospieszny",
            "dawid_tomiczek",
            "dobrochna_przybylek",
            "jakub_nosal",
            "jakub_salamon",
            "jaroslaw_hajduk",
            "julita_bedkowska",
//            "kacper_brezden",
            "kamil_dryzek",
            "kamil_filip",
            "karol_juraszek",
            "konrad_baczynski",
            "krzysztof_stepak",
            "maciej_krasniewski",
            "magdalena_kudyba",
//            "magdalena_plonka",
//            "marcin_pieczka",
            "marek_granowicz",
            "marek_grzybek",
            "marek_mularczyk",
            "marek_pawluch",
//            "marta_gajowczyk",
            "mateusz_korzeniowski",
            "mateusz_wielgosz",
            "michal_malicki",
            "mikolaj_pokrzywa",
            "natalia_adamus",
            "oleksandra_prokhach",
            "patryk_kozlowski",
            "paulina_gawin",
//            "pawel_mietelski",
            "pawel_sztelmach",
            "piotr_marcinkiewicz",
            "rafal_bobik",
            "rafal_czaja",
            "rafal_leoszkiweicz",
            "robert_bystrzanowski",
            "robert_lukas",
            "robert_tomaszek",
            "sebastian_rabiej",
            "sonia_kmiecik",
//            "tomasz_bachminski",
            "tomasz_diakow",
//            "tomasz_staskiewicz",
    };
    public static final String[] PHONES = new String[]{
            "ada_chmielewska",
            "adam_kucharski",
            "adran_dziedzic",
            "aleksandra_krzywanka",
            "andrzej_filipowicz",
            "antoni_nguyen",
            "bartosz_hamielec",
            "daniel_calka",
            "dawid_pospieszny",
            "dawid_tomiczek",
            "dobrochna_przybylek",
            "jakub_nosal",
            "jakub_salamon",
            "jaroslaw_hajduk",
            "juluta_bedkowska",
//            "kacper_brezden",
            "kamil_dryzek",
            "kamil_filip",
            "karol_juraszek",
            "kondar_baczynski",
            "krzysztof_stepak",
            "maciej_krasniewski",
            "magdalena_kudyba",
//            "magdalena_plonka",
//            "marcin_pieczka",
            "marek_granowicz",
            "marek_grzybek",
            "marek_mularczyk",
            "marek_pawluch",
            "marta_gajowczyk",
            "mateusz_korzeniowski",
            "mateusz_wielgosz",
            "michal_malicki",
            "mikolaj_pokrzywa",
            "natalia_adamus",
            "oleksandra_prokhach",
            "patryk_kozlowski",
            "paulina_gawin",
//            "pawel_mietelski",
            "pawel_sztelmach",
            "piotr_marcinkiewicz",
            "rafal_bobik",
            "rafal_czaja",
            "rafal_leoszkiewicz",
            "robert_bystrzanowski",
            "robert_lukas",
            "robert_tomaszek",
            "sebastian_rabiej",
            "sonia_kmiecik",
//            "tomasz_bachminski",
            "tomasz_diakow",
//            "tomasz_staskiewicz",
    };
    public static final String[] LAPTOPS = new String[]{
//                "ada_chmielewska",
            "adam_bartosik",
            "adam_kucharski",
//                "adrian_dziedzic",
            "aleksandra_krzywanska",
            "antoni_nguyen",
            "bartosz_chamielec",
            "bartosz_strach",
            "bartosz_szalaj",
            "daniel_calka",
            "dawid_pospieszny",
            "dawid_tomiczek",
            "dobrochna_przybylek",
//                "jakub_salamon",
            "jaroslaw_hajduk",
//                "julita_bedowska",
            "kamil_dryzek",
            "kamil_kondrat",
            "kamil_tront",
            "karol_juraszek",
            "konrad_baczynski",
            "krzysiek_lakomy",
            "krzysztof_stepak",
            "lukasz_gonczarik",
            "lukasz_zachariasz",
            "maciej_krasniewski",
//                "magdalena_plonka",
            "marcin_pieczka",
            "marek_granowicz",
            "marek_grzybek",
            "marek_mularczyk",
            "marek_pawluch",
//                "marta_gajowczyk",
            "mateusz_wielgosz",
//                "mikolaj_pokrzywa",
            "natalia_adamus",
            "natalia_kosydor",
            "patryk_kozlowski",
            "paulina_gawin",
            "pawel_mietelski",
            "pawel_sztelmach",
            "pawel_wojas",
            "piotr_marcinkiewicz",
//                "przemyslaw_dudycz",
            "rafal_bobik",
            "rafal_leoszkiewicz",
            "robert_bystrzanowski",
            "robert_lukas",
            "robert_tomaszek",
            "sebastian_rabiej",
            "sonia_kmiecik",
            "tomasz_bachminski",
            "tomasz_diakow",
//                "tomasz_staskiewicz";
    };



    private static List<Record> getTestRecords() {
        BufferedReader br = null;
        try {
            br = new BufferedReader(new FileReader("sample_records_list.json"));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

        List<Record> records = new ArrayList<>();
        if(br != null) {
            RecordsList recordsList = new Gson().fromJson(br, RecordsList.class);
            records = recordsList.getList();
        }
        return records;
    }

    public static LearningSet getTestLaptops(String path) {
        BufferedReader br = null;
        String line = "";
        String cvsSplitBy = ";";
        List<models.Record> records = new ArrayList<>();
        try {

            br = new BufferedReader(new FileReader(path));
            int id = 0;
            br.readLine(); //first line contains headers
            while ((line = br.readLine()) != null) {

                // use comma as separator
                line = line.replaceAll("\"", "");
                line = line.replaceAll("\\.", ",");
                line = line.replaceAll("\'", "");
                line = line.replaceAll("\\s+","");
                if(line.isEmpty()) continue;
                String[] record = line.split(cvsSplitBy);
                if(record.length < 8) continue;
                try {
                    String name = record[1];
                    System.out.print(name);
                } catch (ArrayIndexOutOfBoundsException e) { continue; }
                Object o = new Object(id);
                System.out.print(" "+o);
                id++;
                Random r = new Random();
                double desiredStandardDeviation = 25;

                int keyValue = Integer.valueOf(record[record.length-1]);
                double randomKeyValue = 0;
                int keyValueUnchanged = keyValue > 1 ? keyValue : 0;
                switch (keyValue) {
                    case 0:
                        randomKeyValue = r.nextGaussian()*desiredStandardDeviation;
                        break;
                    case 1:
                        randomKeyValue = r.nextGaussian()*desiredStandardDeviation+100;
                        break;
                    case 2:
                        randomKeyValue = r.nextGaussian()*desiredStandardDeviation+200;
                        break;
                    case 3:
                        randomKeyValue = r.nextGaussian()*desiredStandardDeviation+400;
                        break;
                }
                KeyAttribute keyAttribute = new KeyAttribute("ocena");
                keyAttribute.setDiscreteValue(new DiscreteValue(keyValue));
                keyAttribute.setTestValue(keyValue);
                System.out.print(keyAttribute+" ");
                List<AttributeEvaluation> attributeEvaluationList = new ArrayList<>();
                for(int i = 2; i<record.length-1;i++) {
                    switch (i) {
                        case 1: //przekątna ekranu
                            Attribute attribute = new Attribute("ekran");
                            try {
                                String sizeString = "";
                                String sizeValue = record[i].concat(","+record[i+1]);
                                for(Character c : sizeValue.toCharArray()) {
                                    if (c == ',' || c == '.') {
                                        sizeString += ".";
                                    } else {
                                        try {
                                            int digit = Integer.parseInt(c.toString());
                                            sizeString += String.valueOf(digit);
                                        } catch (Exception e) {
                                            //not a number sign nor ',/.'
                                        }
                                    }
                                }
                                Double size = Double.parseDouble(sizeString);
                                if(size < 5 || size > 20) throw new Exception();
                                AttributeEvaluation attributeEvaluation = new AttributeEvaluation(attribute, new DiscreteValue(size));
                                attributeEvaluationList.add(attributeEvaluation);
                            }catch (Exception e) {
                                System.out.println("NIEPOPRAWNA PRZEKĄTNA");
                                System.exit(-1);
                            }
                            break;
                        case 3: //rozdzielczość ekranu
                            try {
                                String resolutionString = record[i];
                                int x = 0,y=0;
                                String xS = "", yS = "";
                                boolean delimeted = false;
                                int tempInt;
                                for(Character c : resolutionString.toCharArray()) {
                                    if (c == 'x') {
                                        x = Integer.valueOf(xS);
                                        delimeted = true;
                                    } else if(!delimeted) {
                                        try {
                                            tempInt = Integer.parseInt(c.toString());
                                            xS += String.valueOf(c);
                                        } catch (Exception e) {}
                                    } else {
                                        try {
                                            tempInt = Integer.parseInt(c.toString());
                                            yS += String.valueOf(c);
                                        }catch (Exception e) {}
                                    }
                                }
                                y = Integer.valueOf(yS);
                                if(x < 100 || y < 100) throw new Exception();
                                //System.out.print("("+xS+"x"+yS+")");
                                AttributeEvaluation attributeEvaluation = new AttributeEvaluation(new Attribute("rozdzielczośćX"), new DiscreteValue(x));
                                AttributeEvaluation attributeEvaluation2 = new AttributeEvaluation(new Attribute("rozdzielczośćY"), new DiscreteValue(y));
                                attributeEvaluationList.add(attributeEvaluation);
                                attributeEvaluationList.add(attributeEvaluation2);
                            } catch (Exception e){
                                System.out.println("NIEPOPRAWNA ROZDZIELCZOŚĆ");
                                System.exit(-1);
                            }
                            break;
                        case 4: //dysk twardy
                            try {
                                String hddString = "";
                                for(Character c : record[i].toCharArray()) {
                                    try {
                                        int val = Integer.parseInt(c.toString());
                                        hddString += c;
                                    } catch (Exception e) { }
                                }
                                Double value = Double.parseDouble(hddString);
                                if(value < 50 || value > 2000) throw new Exception();
                                AttributeEvaluation attributeEvaluation = new AttributeEvaluation(new Attribute("dysk"), new DiscreteValue(value));
                                attributeEvaluationList.add(attributeEvaluation);
                            } catch (Exception e) {
                                System.out.println("NIEPOPRAWNY DYSK");
                                System.exit(-1);
                            }
                            break;
                        case 5: //liczba usb
                            try {
                                String usbS = record[i];
                                int value = 0;
                                try {
                                    value = Integer.parseInt(usbS);
                                } catch (Exception e) { }
                                if(value > 10) throw new Exception();
                                AttributeEvaluation attributeEvaluation = new AttributeEvaluation(new Attribute("usb"), new DiscreteValue(value));
                                attributeEvaluationList.add(attributeEvaluation);
                            } catch (Exception e) {
                                System.out.println("NIEPOPRAWNE USB");
                                System.exit(-1);
                            }
                            break;
                        case 6: //waga
                            try {
                                String weightS = record[i];
                                Double value = 0.0;
                                String valueS = "";
                                for(Character c : weightS.toCharArray()) {
                                    if(c == ',') {
                                        valueS += '.';
                                        continue;
                                    }
                                    try {
                                        int val = Integer.parseInt(c.toString());
                                        valueS += c;
                                    } catch (Exception e) {}
                                }
                                value = Double.parseDouble(valueS);
                                if(value > 10 || value <= 0) throw new Exception();
                                AttributeEvaluation attributeEvaluation = new AttributeEvaluation(new Attribute("waga"), new DiscreteValue(value));
                                attributeEvaluationList.add(attributeEvaluation);
                            } catch (Exception e) {
                                System.out.println("NIEPOPRAWNA WAGA");
                                System.exit(-1);
                            }
                            break;
                        case 7: //ram
                            try {
                                String ramS = record[i];
                                Double value = 0.0;
                                String valueS = "";
                                for(Character c : ramS.toCharArray()) {
                                    try {
                                        int val = Integer.parseInt(c.toString());
                                        valueS += c;
                                    } catch (Exception e) {}
                                }
                                value = Double.parseDouble(valueS);
                                if(value > 33 || value <= 0) throw new Exception();
                                AttributeEvaluation attributeEvaluation = new AttributeEvaluation(new Attribute("ram"), new DiscreteValue(value));
                                attributeEvaluationList.add(attributeEvaluation);
                            } catch (Exception e) {
                                System.out.println("NIEPOPRAWNY RAM");
                                System.exit(-1);
                            }
                            break;
                        case 8: //karta graficzna
                            try {
                                String graphicsS = record[i];
                                Double value = 0.0;
                                String valueS = "";
                                for(Character c : graphicsS.toCharArray()) {
                                    try {
                                        int val = Integer.parseInt(c.toString());
                                        valueS += c;
                                    } catch (Exception e) {}
                                }
                                value = valueS.isEmpty() ? 0 : Double.parseDouble(valueS);
                                if(value > 5000 || value < 0) throw new Exception();
                                AttributeEvaluation attributeEvaluation = new AttributeEvaluation(new Attribute("karta"), new DiscreteValue(value));
                                attributeEvaluationList.add(attributeEvaluation);
                            } catch (Exception e) {
                                System.out.println("NIEPOPRAWNA KARTA");
                                System.exit(-1);
                            }
                            break;
                        case 10: //cpu
                            try {
                                String cpuS = record[i];
                                Double value = 0.0;
                                String valueS = "";
                                int index = 0;
                                while(valueS.length() < 3 && index != cpuS.length()) {
                                    Character c = cpuS.charAt(index);
                                    if(c == ',' || c == '.') valueS += '.';
                                    try {
                                        int val = Integer.parseInt(c.toString());
                                        valueS += c;
                                    } catch (Exception e) {}
                                    index++;
                                }
                                value = valueS.isEmpty() ? 0 : Double.parseDouble(valueS);
                                if(value > 5 || value < 0) throw new Exception();
                                AttributeEvaluation attributeEvaluation = new AttributeEvaluation(new Attribute("cpu"), new DiscreteValue(value));
                                attributeEvaluationList.add(attributeEvaluation);
                            } catch (Exception e) {
                                System.out.println("NIEPOPRAWNE CPU");
                                System.exit(-1);
                            }
                            break;
                        case 11: //liczba rdzeni
                            try {
                                Double value = Double.parseDouble(record[i]);

                                if(value > 5 || value <= 0) throw new Exception();
                                AttributeEvaluation attributeEvaluation = new AttributeEvaluation(new Attribute("rdzeni"), new DiscreteValue(value));
                                attributeEvaluationList.add(attributeEvaluation);
                            } catch (Exception e) {
                                System.out.println("NIEPOPRAWNE RDZENIE");
                                System.exit(-1);
                            }
                            break;
                        case 12: //cena
                            try {
                                Double value = 0.0;
                                String valueS = "";
                                for(Character c : (record[i]/*+record[i+1]*/).toCharArray()) {
                                    try {
                                        int val = Integer.parseInt(c.toString());
                                        valueS += c;
                                    } catch (Exception e) {}
                                }
                                value = Double.parseDouble(valueS);
                                if(value > 10000 || value <= 0) throw new Exception();
                                AttributeEvaluation attributeEvaluation = new AttributeEvaluation(new Attribute("cena"), new DiscreteValue(value));
                                attributeEvaluationList.add(attributeEvaluation);
                            } catch (Exception e) {
                                System.out.println("NIEPOPRAWNA CENA");
                                System.exit(-1);
                            }
                            break;
                    }
                }
                System.out.print("wartości atrybutów:");
                for(AttributeEvaluation attributeEvaluation : attributeEvaluationList) {
                    System.out.print(" " + attributeEvaluation + " ");
                }
                System.out.println();
                records.add(new models.Record(o,attributeEvaluationList,keyAttribute));
            }

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (br != null) {
                try {
                    br.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        return new LearningSet(records);
    }

    public static LearningSet getTestPhones(String path) {
        BufferedReader br = null;
        String line = "";
        String cvsSplitBy = ";";
        List<models.Record> records = new ArrayList<>();
        try {

            br = new BufferedReader(new FileReader(path));
            int id = 0;
            br.readLine(); //first line contains headers
            while ((line = br.readLine()) != null) {

                // use comma as separator
                line = line.replaceAll("\"", "");
                line = line.replaceAll("\\.", ",");
                line = line.replaceAll("\'", "");
                line = line.replaceAll("\\s+","");
                if(line.isEmpty()) continue;
                String[] record = line.split(cvsSplitBy);
                if(record.length < 8) continue;
                try {
                    String name = record[1];
                    System.out.print(name);
                } catch (ArrayIndexOutOfBoundsException e) { continue; }
                Object o = new Object(id);
                System.out.print(" "+o);
                id++;
                Random r = new Random();
                double desiredStandardDeviation = 25;

                int keyValue = 0;
                try {
                    keyValue = Integer.valueOf(record[record.length-1]);
                } catch (Exception e) {

                }
                double randomKeyValue = 0;
                int keyValueUnchanged = keyValue > 1 ? keyValue : 0;
                switch (keyValue) {
                    case 0:
                        randomKeyValue = r.nextGaussian()*desiredStandardDeviation;
                        break;
                    case 1:
                        randomKeyValue = r.nextGaussian()*desiredStandardDeviation+100;
                        break;
                    case 2:
                        randomKeyValue = r.nextGaussian()*desiredStandardDeviation+200;
                        break;
                    case 3:
                        randomKeyValue = r.nextGaussian()*desiredStandardDeviation+400;
                        break;
                }
                KeyAttribute keyAttribute = new KeyAttribute("ocena");
                keyAttribute.setDiscreteValue(new DiscreteValue(keyValue));
                keyAttribute.setTestValue(keyValue);
                System.out.print(keyAttribute+" ");
                List<AttributeEvaluation> attributeEvaluationList = new ArrayList<>();
                for(int i = 2; i<record.length-1;i++) {
                    switch (i) {
                        case 1: //przekątna ekranu
                            Attribute attribute = new Attribute("ekran");
                            try {
                                String sizeString = "";
                                String sizeValue = record[i].concat(","+record[i+1]);
                                for(Character c : sizeValue.toCharArray()) {
                                    if (c == ',' || c == '.') {
                                        sizeString += ".";
                                    } else {
                                        try {
                                            int digit = Integer.parseInt(c.toString());
                                            sizeString += String.valueOf(digit);
                                        } catch (Exception e) {
                                            //not a number sign nor ',/.'
                                        }
                                    }
                                }
                                Double size = Double.parseDouble(sizeString);
                                if(size < 5 || size > 20) throw new Exception();
                                AttributeEvaluation attributeEvaluation = new AttributeEvaluation(attribute, new DiscreteValue(size));
                                attributeEvaluationList.add(attributeEvaluation);
                            }catch (Exception e) {
                                System.out.println("NIEPOPRAWNA PRZEKĄTNA");
                                System.exit(-1);
                            }
                            break;
                        case 3: //rozdzielczość ekranu
                            try {
                                String resolutionString = record[i];
                                int x = 0,y=0;
                                String xS = "", yS = "";
                                boolean delimeted = false;
                                int tempInt;
                                for(Character c : resolutionString.toCharArray()) {
                                    if (c == 'x') {
                                        x = Integer.valueOf(xS);
                                        delimeted = true;
                                    } else if(!delimeted) {
                                        try {
                                            tempInt = Integer.parseInt(c.toString());
                                            xS += String.valueOf(c);
                                        } catch (Exception e) {}
                                    } else {
                                        try {
                                            tempInt = Integer.parseInt(c.toString());
                                            yS += String.valueOf(c);
                                        }catch (Exception e) {}
                                    }
                                }
                                y = Integer.valueOf(yS);
                                if(x < 100 || y < 100) throw new Exception();
                                //System.out.print("("+xS+"x"+yS+")");
                                AttributeEvaluation attributeEvaluation = new AttributeEvaluation(new Attribute("rozdzielczośćX"), new DiscreteValue(x));
                                AttributeEvaluation attributeEvaluation2 = new AttributeEvaluation(new Attribute("rozdzielczośćY"), new DiscreteValue(y));
                                attributeEvaluationList.add(attributeEvaluation);
                                attributeEvaluationList.add(attributeEvaluation2);
                            } catch (Exception e){
                                System.out.println("NIEPOPRAWNA ROZDZIELCZOŚĆ");
                                System.exit(-1);
                            }
                            break;
                        case 4: //pamiec
                            try {
                                String hddString = "";
                                for(Character c : record[i].toCharArray()) {
                                    try {
                                        int val = Integer.parseInt(c.toString());
                                        hddString += c;
                                    } catch (Exception e) { }
                                }
                                Double value = Double.parseDouble(hddString);
                                if(value < 0 || value > 500) throw new Exception();
                                AttributeEvaluation attributeEvaluation = new AttributeEvaluation(new Attribute("pamięć"), new DiscreteValue(value));
                                attributeEvaluationList.add(attributeEvaluation);
                            } catch (Exception e) {
                                System.out.println("NIEPOPRAWNA PAMIĘĆ");
                                System.exit(-1);
                            }
                            break;
                        case 5: //bateria
                            String bateryString = "";
                            try {
                                for(Character c : record[i].toCharArray()) {
                                    try {
                                        int val = Integer.parseInt(c.toString());
                                        bateryString += c;
                                    } catch (Exception e) { }
                                }
                                int value = Integer.parseInt(bateryString);
                                if(value < 0 || value > 10000 ) throw new Exception();
                                AttributeEvaluation attributeEvaluation = new AttributeEvaluation(new Attribute("usb"), new DiscreteValue(value));
                                attributeEvaluationList.add(attributeEvaluation);
                            } catch (Exception e) {
                                System.out.println("NIEPOPRAWNE USB");
                                System.exit(-1);
                            }
                            break;
                        case 6: //waga
                            String weightString = "";
                            try {
                                for(Character c : record[i].toCharArray()) {
                                    try {
                                        int val = Integer.parseInt(c.toString());
                                        weightString += c;
                                    } catch (Exception e) { }
                                }
                                int value = Integer.parseInt(weightString);
                                   if(value > 500|| value <= 0) throw new Exception();
                                AttributeEvaluation attributeEvaluation = new AttributeEvaluation(new Attribute("waga"), new DiscreteValue(value));
                                attributeEvaluationList.add(attributeEvaluation);
                            } catch (Exception e) {
                                System.out.println("NIEPOPRAWNA WAGA");
                                System.exit(-1);
                            }
                            break;
                        case 7: //ram
                            try {
                                String ramS = record[i];
                                Double value = 0.0;
                                String valueS = "";
                                for(Character c : ramS.toCharArray()) {
                                    if(c.equals('.') || c.equals(',')) {
                                        valueS += "."; continue;
                                    }
                                    try {
                                        int val = Integer.parseInt(c.toString());
                                        valueS += c;
                                    } catch (Exception e) {}
                                }
                                value = Double.parseDouble(valueS);
                                if(value > 33 || value <= 0) throw new Exception();
                                AttributeEvaluation attributeEvaluation = new AttributeEvaluation(new Attribute("ram"), new DiscreteValue(value));
                                attributeEvaluationList.add(attributeEvaluation);
                            } catch (Exception e) {
                                System.out.println("NIEPOPRAWNY RAM");
                                System.exit(-1);
                            }
                            break;
                        case 8: //aparat
                            try {
                                String cameraString = "";
                                for(Character c : record[i].toCharArray()) {
                                    if (c == ',' || c == '.') {
                                        cameraString += ".";
                                    } else {
                                        try {
                                            int digit = Integer.parseInt(c.toString());
                                            cameraString += String.valueOf(digit);
                                        } catch (Exception e) {
                                            //not a number sign nor ',/.'
                                        }
                                    }
                                }
                                Double mpx = Double.parseDouble(cameraString);
                                if(mpx < 0 || mpx > 50) throw new Exception();
                                AttributeEvaluation attributeEvaluation = new AttributeEvaluation(new Attribute("aparat"), new DiscreteValue(mpx));
                                attributeEvaluationList.add(attributeEvaluation);
                            } catch (Exception e) {
                                System.out.println("NIEPOPRAWNY APARAT");
                                System.exit(-1);
                            }
                            break;
                        case 9: //grubość
                            try {
                                String thicknessStirng = "";
                                for(Character c : record[i].toCharArray()) {
                                    if (c == ',' || c == '.') {
                                        thicknessStirng += ".";
                                    } else {
                                        try {
                                            int digit = Integer.parseInt(c.toString());
                                            thicknessStirng += String.valueOf(digit);
                                        } catch (Exception e) {
                                            //not a number sign nor ',/.'
                                        }
                                    }
                                }
                                Double mpx = Double.parseDouble(thicknessStirng);
                                if(mpx < 0 || mpx > 50) throw new Exception();
                                AttributeEvaluation attributeEvaluation = new AttributeEvaluation(new Attribute("grubość"), new DiscreteValue(mpx));
                                attributeEvaluationList.add(attributeEvaluation);
                            } catch (Exception e) {
                                System.out.println("NIEPOPRAWNA GRUBOŚĆ");
                                System.exit(-1);
                            }
                            break;
                        case 10: //cpu
                            try {
                                String cpuS = record[i];
                                Double value = 0.0;
                                String valueS = "";
                                int index = 0;
                                while(valueS.length() < 3 && index != cpuS.length()) {
                                    Character c = cpuS.charAt(index);
                                    if(c == ',' || c == '.') valueS += '.';
                                    try {
                                        int val = Integer.parseInt(c.toString());
                                        valueS += c;
                                    } catch (Exception e) {}
                                    index++;
                                }
                                value = valueS.isEmpty() ? 0 : Double.parseDouble(valueS);
                                if(value > 5 || value < 0) throw new Exception();
                                AttributeEvaluation attributeEvaluation = new AttributeEvaluation(new Attribute("cpu"), new DiscreteValue(value));
                                attributeEvaluationList.add(attributeEvaluation);
                            } catch (Exception e) {
                                System.out.println("NIEPOPRAWNE CPU");
                                System.exit(-1);
                            }
                            break;
                        case 12: //liczba rdzeni
                            try {
                                Double value = Double.parseDouble(record[i]);

                                if(value > 6 || value <= 0) throw new Exception();
                                AttributeEvaluation attributeEvaluation = new AttributeEvaluation(new Attribute("rdzeni"), new DiscreteValue(value));
                                attributeEvaluationList.add(attributeEvaluation);
                            } catch (Exception e) {
                                System.out.println("NIEPOPRAWNE RDZENIE");
                                System.exit(-1);
                            }
                            break;
                        case 13: //cena
                            try {
                                Double value = 0.0;
                                String valueS = "";
                                for(Character c : (record[i]/*+record[i+1]*/).toCharArray()) {
                                    try {
                                        int val = Integer.parseInt(c.toString());
                                        valueS += c;
                                    } catch (Exception e) {}
                                }
                                value = Double.parseDouble(valueS);
                                if(value > 10000 || value <= 0) throw new Exception();
                                AttributeEvaluation attributeEvaluation = new AttributeEvaluation(new Attribute("cena"), new DiscreteValue(value));
                                attributeEvaluationList.add(attributeEvaluation);
                            } catch (Exception e) {
                                System.out.println("NIEPOPRAWNA CENA");
                                System.exit(-1);
                            }
                            break;
                    }
                }
                System.out.print("wartości atrybutów:");
                for(AttributeEvaluation attributeEvaluation : attributeEvaluationList) {
                    System.out.print(" " + attributeEvaluation + " ");
                }
                System.out.println();
                records.add(new models.Record(o,attributeEvaluationList,keyAttribute));
            }

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (br != null) {
                try {
                    br.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        return new LearningSet(records);
    }

    public static LearningSet getTestTablets(String path) {
        BufferedReader br = null;
        String line = "";
        String cvsSplitBy = ";";
        List<models.Record> records = new ArrayList<>();
        try {

            br = new BufferedReader(new FileReader(path));
            int id = 0;
            br.readLine(); //first line contains headers
            while ((line = br.readLine()) != null) {

                // use comma as separator
                line = line.replaceAll("\"", "");
                line = line.replaceAll("\\.", ",");
                line = line.replaceAll("\'", "");
                line = line.replaceAll("\\s+","");
                if(line.isEmpty()) continue;
                String[] record = line.split(cvsSplitBy);
                if(record.length < 8) continue;
                try {
                    String name = record[1];
                    System.out.print(name);
                } catch (ArrayIndexOutOfBoundsException e) { continue; }
                Object o = new Object(id);
                System.out.print(" "+o);
                id++;
                Random r = new Random();
                double desiredStandardDeviation = 25;

                int keyValue = 0;
                try {
                    keyValue = Integer.valueOf(record[record.length-1]);
                } catch (Exception e) {

                }
                double randomKeyValue = 0;
                int keyValueUnchanged = keyValue > 1 ? keyValue : 0;
                switch (keyValue) {
                    case 0:
                        randomKeyValue = r.nextGaussian()*desiredStandardDeviation;
                        break;
                    case 1:
                        randomKeyValue = r.nextGaussian()*desiredStandardDeviation+100;
                        break;
                    case 2:
                        randomKeyValue = r.nextGaussian()*desiredStandardDeviation+200;
                        break;
                    case 3:
                        randomKeyValue = r.nextGaussian()*desiredStandardDeviation+400;
                        break;
                }
                KeyAttribute keyAttribute = new KeyAttribute("ocena");
                keyAttribute.setDiscreteValue(new DiscreteValue(keyValue));
                keyAttribute.setTestValue(keyValue);
                System.out.print(keyAttribute+" ");
                List<AttributeEvaluation> attributeEvaluationList = new ArrayList<>();
                for(int i = 2; i<record.length-1;i++) {
                    switch (i) {
                        case 1: //przekątna ekranu
                            Attribute attribute = new Attribute("ekran");
                            try {
                                String sizeString = "";
                                String sizeValue = record[i].concat(","+record[i+1]);
                                for(Character c : sizeValue.toCharArray()) {
                                    if (c == ',' || c == '.') {
                                        sizeString += ".";
                                    } else {
                                        try {
                                            int digit = Integer.parseInt(c.toString());
                                            sizeString += String.valueOf(digit);
                                        } catch (Exception e) {
                                            //not a number sign nor ',/.'
                                        }
                                    }
                                }
                                Double size = Double.parseDouble(sizeString);
                                if(size < 5 || size > 20) throw new Exception();
                                AttributeEvaluation attributeEvaluation = new AttributeEvaluation(attribute, new DiscreteValue(size));
                                attributeEvaluationList.add(attributeEvaluation);
                            }catch (Exception e) {
                                System.out.println("NIEPOPRAWNA PRZEKĄTNA");
                                System.exit(-1);
                            }
                            break;
                        case 3: //rozdzielczość ekranu
                            try {
                                String resolutionString = record[i];
                                int x = 0,y=0;
                                String xS = "", yS = "";
                                boolean delimeted = false;
                                int tempInt;
                                for(Character c : resolutionString.toCharArray()) {
                                    if (c == 'x') {
                                        x = Integer.valueOf(xS);
                                        delimeted = true;
                                    } else if(!delimeted) {
                                        try {
                                            tempInt = Integer.parseInt(c.toString());
                                            xS += String.valueOf(c);
                                        } catch (Exception e) {}
                                    } else {
                                        try {
                                            tempInt = Integer.parseInt(c.toString());
                                            yS += String.valueOf(c);
                                        }catch (Exception e) {}
                                    }
                                }
                                y = Integer.valueOf(yS);
                                if(x < 100 || y < 100) throw new Exception();
                                //System.out.print("("+xS+"x"+yS+")");
                                AttributeEvaluation attributeEvaluation = new AttributeEvaluation(new Attribute("rozdzielczośćX"), new DiscreteValue(x));
                                AttributeEvaluation attributeEvaluation2 = new AttributeEvaluation(new Attribute("rozdzielczośćY"), new DiscreteValue(y));
                                attributeEvaluationList.add(attributeEvaluation);
                                attributeEvaluationList.add(attributeEvaluation2);
                            } catch (Exception e){
                                System.out.println("NIEPOPRAWNA ROZDZIELCZOŚĆ");
                                System.exit(-1);
                            }
                            break;
                        case 4: //pamiec
                            try {
                                String hddString = "";
                                for(Character c : record[i].toCharArray()) {
                                    try {
                                        int val = Integer.parseInt(c.toString());
                                        hddString += c;
                                    } catch (Exception e) { }
                                }
                                Double value = Double.parseDouble(hddString);
                                if(value < 0 || value > 500) throw new Exception();
                                AttributeEvaluation attributeEvaluation = new AttributeEvaluation(new Attribute("pamięć"), new DiscreteValue(value));
                                attributeEvaluationList.add(attributeEvaluation);
                            } catch (Exception e) {
                                System.out.println("NIEPOPRAWNA PAMIĘĆ");
                                System.exit(-1);
                            }
                            break;
                        case 5: //bateria
                            String bateryString = "";
                            try {
                                for(Character c : record[i].toCharArray()) {
                                    try {
                                        int val = Integer.parseInt(c.toString());
                                        bateryString += c;
                                    } catch (Exception e) { }
                                }
                                int value = Integer.parseInt(bateryString);
                                if(value < 0 || value > 15000 ) throw new Exception();
                                AttributeEvaluation attributeEvaluation = new AttributeEvaluation(new Attribute("usb"), new DiscreteValue(value));
                                attributeEvaluationList.add(attributeEvaluation);
                            } catch (Exception e) {
                                System.out.println("NIEPOPRAWNA BATERIA");
                                System.exit(-1);
                            }
                            break;
                        case 6: //waga
                            String weightString = "";
                            try {
                                for(Character c : record[i].toCharArray()) {
                                    try {
                                        int val = Integer.parseInt(c.toString());
                                        weightString += c;
                                    } catch (Exception e) { }
                                }
                                int value = Integer.parseInt(weightString);
                                if(value > 1000|| value <= 0) throw new Exception();
                                AttributeEvaluation attributeEvaluation = new AttributeEvaluation(new Attribute("waga"), new DiscreteValue(value));
                                attributeEvaluationList.add(attributeEvaluation);
                            } catch (Exception e) {
                                System.out.println("NIEPOPRAWNA WAGA");
                                System.exit(-1);
                            }
                            break;
                        case 7: //ram
                            try {
                                String ramS = record[i];
                                Double value = 0.0;
                                String valueS = "";
                                for(Character c : ramS.toCharArray()) {
                                    if(c.equals('.') || c.equals(',')) {
                                        valueS += "."; continue;
                                    }
                                    try {
                                        int val = Integer.parseInt(c.toString());
                                        valueS += c;
                                    } catch (Exception e) {}
                                }
                                value = Double.parseDouble(valueS);
                                if(value > 33 || value <= 0) throw new Exception();
                                AttributeEvaluation attributeEvaluation = new AttributeEvaluation(new Attribute("ram"), new DiscreteValue(value));
                                attributeEvaluationList.add(attributeEvaluation);
                            } catch (Exception e) {
                                System.out.println("NIEPOPRAWNY RAM");
                                System.exit(-1);
                            }
                            break;
                        case 8: //aparat
                            try {
                                String cameraString = "";
                                for(Character c : record[i].toCharArray()) {
                                    if (c == ',' || c == '.') {
                                        cameraString += ".";
                                    } else {
                                        try {
                                            int digit = Integer.parseInt(c.toString());
                                            cameraString += String.valueOf(digit);
                                        } catch (Exception e) {
                                            //not a number sign nor ',/.'
                                        }
                                    }
                                }
                                Double mpx = Double.parseDouble(cameraString);
                                if(mpx < 0 || mpx > 50) throw new Exception();
                                AttributeEvaluation attributeEvaluation = new AttributeEvaluation(new Attribute("aparat"), new DiscreteValue(mpx));
                                attributeEvaluationList.add(attributeEvaluation);
                            } catch (Exception e) {
                                System.out.println("NIEPOPRAWNY APARAT");
                                System.exit(-1);
                            }
                            break;
                        case 9: //cpu
                            try {
                                String cpuS = record[i];
                                Double value = 0.0;
                                String valueS = "";
                                int index = 0;
                                while(valueS.length() < 3 && index != cpuS.length()) {
                                    Character c = cpuS.charAt(index);
                                    if(c == ',' || c == '.') valueS += '.';
                                    try {
                                        int val = Integer.parseInt(c.toString());
                                        valueS += c;
                                    } catch (Exception e) {}
                                    index++;
                                }
                                value = valueS.isEmpty() ? 0 : Double.parseDouble(valueS);
                                if(value > 10 || value < 0) throw new Exception();
                                AttributeEvaluation attributeEvaluation = new AttributeEvaluation(new Attribute("cpu"), new DiscreteValue(value));
                                attributeEvaluationList.add(attributeEvaluation);
                            } catch (Exception e) {
                                System.out.println("NIEPOPRAWNE CPU");
                                System.exit(-1);
                            }
                            break;
                        case 11: //liczba rdzeni
                            try {
                            String val = record[i];
                                Double value = Double.parseDouble(record[i]);

                                if(value > 8 || value <= 0) throw new Exception();
                                AttributeEvaluation attributeEvaluation = new AttributeEvaluation(new Attribute("rdzeni"), new DiscreteValue(value));
                                attributeEvaluationList.add(attributeEvaluation);
                            } catch (Exception e) {
                                System.out.println("NIEPOPRAWNE RDZENIE");
                                System.exit(-1);
                            }
                            break;
                        case 12: //cena
                            try {
                                Double value = 0.0;
                                String valueS = "";
                                for(Character c : (record[i]/*+record[i+1]*/).toCharArray()) {
                                    try {
                                        int val = Integer.parseInt(c.toString());
                                        valueS += c;
                                    } catch (Exception e) {}
                                }
                                value = Double.parseDouble(valueS);
                                if(value > 10000 || value <= 0) throw new Exception();
                                AttributeEvaluation attributeEvaluation = new AttributeEvaluation(new Attribute("cena"), new DiscreteValue(value));
                                attributeEvaluationList.add(attributeEvaluation);
                            } catch (Exception e) {
                                System.out.println("NIEPOPRAWNA CENA");
                                System.exit(-1);
                            }
                            break;
                    }
                }
                System.out.print("wartości atrybutów:");
                for(AttributeEvaluation attributeEvaluation : attributeEvaluationList) {
                    System.out.print(" " + attributeEvaluation + " ");
                }
                System.out.println();
                records.add(new models.Record(o,attributeEvaluationList,keyAttribute));
            }

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (br != null) {
                try {
                    br.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        return new LearningSet(records);
    }

    public class Record extends models.Record {

        public Record(Object item, List attributeEvaluations, KeyAttribute o) {
            super(item, attributeEvaluations, o);
        }
    }

    public static class RecordsList {
        private List<Record> records;

        public List<Record> getList() {
            return records;
        }

        public void setRecords(List<Record> records) {
            this.records = records;
        }
    }

    public static LearningSet getTestLearningSet() {
        BufferedReader br = null;
        try {
            br = new BufferedReader(new FileReader("/home/jsalamon/Praca Inżynierska/Projekt - moduł wsp. dec./Praca_Inzynierska_implementacja/src/tests/assets/json/sample_records2.json"));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        if(br != null) {


            List<models.Record> object = new Gson().fromJson(br, new TypeToken<List<models.Record<DiscreteValue>>>(){}.getType());
            return new LearningSet(object);
        }
        return null;
    }
}