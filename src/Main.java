import algorithms.Algorithm;
import control.Controller;
import control.Fuzzyficator;
import lists.LearningSet;
import logic.GeodelLogic;
import logic.Larsen;
import models.Record;
import operators.Approximation;
import operators.Operator;
import tests.Test;

import java.io.Console;
import java.io.FileWriter;
import java.io.IOException;
import java.util.*;

public class Main {

    public static void main(String[] args) {
        LearningSet learningSet = Test.getTestLearningSet();
        String output = "";
        for (String file : Test.PHONES) {
            System.out.println("\n"+file+"\n");
            LearningSet recordsCsv = Test.getTestPhones("/home/jsalamon/Praca Inżynierska/Projekt - moduł wsp. dec./Praca_Inzynierska_implementacja/src/tests/assets/csv/phones/" + file + ".csv");
            Controller controller = new Controller(new Operator(new GeodelLogic.TNorm(), new GeodelLogic.SNorm(), new Larsen.Implication(), Approximation.DOWN));
            LearningSet ls = new LearningSet(recordsCsv);
            ls.removeAll(recordsCsv.subList(9,15));
            controller.setLearningSet(ls);
            controller.generateRules();

            List<Record> r = Fuzzyficator.fuzzyInput(recordsCsv.subList(9, 15), Algorithm.getAttributeSpecifications(recordsCsv), Algorithm.getKeyAttributeSpecifications(recordsCsv));

            Collections.sort(r, new Comparator<Record>() {
                @Override
                public int compare(Record o1, Record o2) {
                    double value1 = controller.calculateOutputValue(o1).getValue();
                    double value2 = controller.calculateOutputValue(o2).getValue();
                    if(value1 > value2) return 1;
                    if(value1 < value2) return -1;
                    return 0;
                }
            });
            int i = 0;
            output += file+',';
            for(Record record : r) {
                output += String.valueOf(record.getKeyAttribute().getTestValue()) + ",";
            }
            output += "\n";
        }
        Calendar calendar = Calendar.getInstance();
        String fileName = String.valueOf(calendar.get(Calendar.HOUR))+String.valueOf(calendar.get(Calendar.MINUTE));
        String filePath = "/home/jsalamon/Desktop/"+fileName+".csv";
        try {
            FileWriter writer = new FileWriter(filePath);
            writer.write(output);
            writer.flush();
            writer.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
