package operators;


import logic.interfaces.Implication;
import logic.interfaces.SNorm;
import logic.interfaces.TNorm;
import models.values.Value;
import org.jetbrains.annotations.Nullable;

public class
Operator {

    final private TNorm tNormOperator;
    final private SNorm sNormOperator;
    final private Implication implicationOperator;
    final private Approximation approximation;

    public Operator(TNorm tNormOperator, SNorm sNormOperator, Implication implicationOperator, Approximation approximation) {
        this.tNormOperator = tNormOperator;
        this.sNormOperator = sNormOperator;
        this.implicationOperator = implicationOperator;
        this.approximation = approximation;
    }

    public <V extends Value> Value calculateTNorm(@Nullable V a, @Nullable V b) {
        return this.tNormOperator.getValue(a, b);
    }

    public <V extends Value> Value calculateSNorm(@Nullable V a, @Nullable V b) {
        return this.sNormOperator.getValue(a, b);
    }

    public <V extends Value> Value calculateImplication(@Nullable V a, @Nullable V b) {
        return this.implicationOperator.getValue(a, b);
    }

    public Approximation getApproximation() {
        return approximation;
    }
}


